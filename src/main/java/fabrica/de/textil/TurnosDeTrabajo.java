public class TurnosDeTrabajo{
    private String fecha;
    private Integer horaIngresoHs;
    private Integer horaIngresoMin;
    private Integer horaEgresoHs;
    private Integer horaEgresoMin;

    public TurnosDeTrabajo(String vfecha, Integer vingresoHs, Integer vingresoMin, Integer vegresoHs, Integer vegresoMin){
        this.fecha = vfecha;
        this.horaIngresoHs = vingresoHs;
        this.horaIngresoMin = vingresoMin;
        this.horaEgresoHs = vegresoHs;
        this.horaEgresoMin = vegresoMin;
    }

    public void setFecha(String vFecha){
        this.fecha = vFecha;
    }
    public String getFecha(){
        return fecha;
    }

    public void setHoraIngresoHs(Integer vHoraingresoHs){
        this.horaIngresoHs = vHoraingresoHs;
    }
    public Integer getHoraIngresoHs(){
        return horaIngresoHs;
    }

    public void setHoraIngresoMin(Integer vHoraIngresoMin){
        this.horaIngresoMin = vHoraIngresoMin;
    }
    public Integer getHoraIngresoMin(){
        return horaIngresoMin;
    }

    public void setHoraEgresoHs(Integer vHoraEgresoHs){
        this.horaEgresoHs = vHoraEgresoHs;
    }
    public Integer getHoraEgresoHs(){
        return horaEgresoHs;
    }

    public void setHoraEgresoMin(Integer vHoraEgresoMin){
        this.horaEgresoMin = vHoraEgresoMin;
    }
    public Integer getHoraEgresoMin(){
        return horaEgresoMin;
    }

    public void mostrarPorPantallaTurnos(){
        System.out.println("Fecha: "+fecha+".");
        System.out.println("Hora de Ingreso: "+horaIngresoHs+":"+horaIngresoMin+"hs.");
        System.out.println("Hora de Egreso: "+horaEgresoHs+":"+horaEgresoMin+"hs.");
    }
}