public class Empleado{
    private String nombre;
    private String apellido;
    private Double costoPorHoraDeTrabajo;
    private Double sueldo;

    public Empleado(String xnombre, String xapellido, Double xcostoPorHoraDeTrabajo){
        this.apellido = xapellido;
        this.nombre = xnombre;
        this.costoPorHoraDeTrabajo = xcostoPorHoraDeTrabajo;
    }

    public void setNombre(String vNombre){
        this.nombre = vNombre;
    }
    public String getNombre(){
        return nombre;
    }

    public void setApellido(String vApellido){
        this.apellido = vApellido;
    }
    public String getApellido(){
        return apellido;
    }

    public void setCostoPorHoraDeTrabajo(Double vCostoPorHoraDeTrabajo){
        this.costoPorHoraDeTrabajo = vCostoPorHoraDeTrabajo;
    }
    public Double getCostoPorHoraDeTrabajo(){
        return costoPorHoraDeTrabajo;
    }

    public void calcularSueldo(Integer ingresoHs,Integer ingresoMin,Integer egresoHs,Integer egresoMin){
        this.sueldo = ((egresoHs + egresoMin/60.0) - (ingresoHs + ingresoMin/60.0)) * costoPorHoraDeTrabajo;
    }
    public Double getSueldo(){
        return sueldo;
    }

    public void mostarPorPantalla(){
        System.out.println("Nombre: "+nombre);
        System.out.println("Apellido: "+apellido);
        System.out.println("Sueldo: $"+sueldo);
    }

}