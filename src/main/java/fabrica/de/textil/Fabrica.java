import java.util.ArrayList;

public class Fabrica{
    ArrayList<Empleado> listaDeEmpleados = new ArrayList<Empleado>();
    ArrayList<TurnosDeTrabajo> listaDeTurnosDeTrabajo = new ArrayList<TurnosDeTrabajo>();
    Integer i;

    public void agregarEmpleados(String vnombre, String vapellido, Double vcostoDeHoras, String vfecha, Integer vingresoHs, Integer vingresoMin, Integer vegresoHs, Integer vegresoMin){
        Empleado empleado = new Empleado(vnombre, vapellido, vcostoDeHoras);
        empleado.calcularSueldo(vingresoHs, vingresoMin, vegresoHs, vegresoMin);
        listaDeEmpleados.add(empleado);
        TurnosDeTrabajo turno = new TurnosDeTrabajo(vfecha, vingresoHs, vingresoMin, vegresoHs, vegresoMin);
        listaDeTurnosDeTrabajo.add(turno);
        //empleado.mostarPorPantalla();
        //turno.mostrarPorPantallaTurnos();
    }

    public void mostrarEmpleados(){
        for(i=0; i<listaDeEmpleados.size(); i++){
            listaDeEmpleados.get(i).mostarPorPantalla();
        }
    }

    public void mostrarRegistroDeTurnosDeTrabajo(){
        for(i=0; i<listaDeEmpleados.size(); i++){
            listaDeEmpleados.get(i).mostarPorPantalla();
            listaDeTurnosDeTrabajo.get(i).mostrarPorPantallaTurnos();
        }
    }
}